<?php

$lang['administrators_app_description'] = '관리자 앱을 사용하면 시스템의 사용자 그룹에 특정 앱에 대한 액세스 권한을 부여 할 수 있습니다.';
$lang['administrators_app_name'] = '관리자';
